﻿using System;

namespace TalentSeeDiscountCalculator.Data
{
    public interface IDiscountRepository
    {
        DiscountRate GetRate();
        DiscountSupplement GetSupplementaRate(DiscountSupplementType type);
    }

    public class DiscountRepository : IDiscountRepository
    {
        public DiscountRate GetRate()
        {
            return new DiscountRate
            {
                Rate = new decimal(20.0)
            };
        }

        public DiscountSupplement GetSupplementaRate(DiscountSupplementType type)
        {
            var result = new DiscountSupplement();
            switch (type)
            {
                case DiscountSupplementType.Enterprise:
                    result.SupplementalRate = new decimal(5);
                    break;
                case DiscountSupplementType.SmallMed:
                    result.SupplementalRate = new decimal(3);
                    break;
                case DiscountSupplementType.Standard:
                    result.SupplementalRate = 0;
                    break;
                default:
                    result.SupplementalRate = 0;
                    break;
            }

            return result;
        }
    }

    public class DiscountRate
    {
        public Decimal Rate { get; set; }
    }

    public class DiscountSupplement
    {    
        public decimal SupplementalRate { get; set; }
    }

    public enum DiscountSupplementType
    {
        Enterprise = 1,
        SmallMed = 2,
        Standard = 3
    }
}
