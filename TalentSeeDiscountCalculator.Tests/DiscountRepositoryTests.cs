﻿using NUnit.Framework;
using TalentSeeDiscountCalculator.Data;

namespace TalentSeeDiscountCalculator
{
    [TestFixture]
    public class DiscountRepositoryTests
    {
        [Test]
        public void GetRateReturnsRate()
        {
            var repo = new DiscountRepository();

            var actual = repo.GetRate();

            Assert.IsInstanceOf<DiscountRate>(actual);
        }
    }
}