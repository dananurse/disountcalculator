﻿using System;
using Moq;
using NUnit.Framework;
using TalentSeeDiscountCalculator.Data;
using TalentSeeDiscountCalculator.Logic;

namespace TalentSeeDiscountCalculator
{
    [TestFixture]
    public class CalculatorTests
    {
        [Test]
        public void CalculateDiscountCalculatesCorrectValue()
        {
            IDiscountRepository repo = new DiscountRepository();
            var calculator = new Calculator(repo);
            var rate = repo.GetRate().Rate;

            var result = calculator.GetDiscountTotal(12);

            var expected = (decimal)12.0 * (rate / (100 - rate));

            Assert.AreEqual(expected, result);
        }

        [Test]
        public void CalculateDiscountCallsRateRepository()
        {
            var mockRepo = new Mock<IDiscountRepository>();
            mockRepo.Setup(c => c.GetRate()).Returns(new DiscountRate { Rate = 20 });
            mockRepo.Setup(c => c.GetSupplementaRate(It.IsAny<DiscountSupplementType>()))
                    .Returns(new DiscountSupplement{ SupplementalRate = 0});

            var calculator = new Calculator(mockRepo.Object);

            var result = calculator.GetDiscountTotal(12);

            mockRepo.Verify(m => m.GetRate(), Times.Once());
        }

        [Test]
        public void DiscountGetsRounded()
        {
            var mockRepo = new Mock<IDiscountRepository>();
            mockRepo.Setup(c => c.GetRate()).Returns(new DiscountRate { Rate = new decimal(17.5) });
            mockRepo.Setup(c => c.GetSupplementaRate(It.IsAny<DiscountSupplementType>()))
                    .Returns(new DiscountSupplement{ SupplementalRate = 0});

            var calculator = new Calculator(mockRepo.Object);

            var result = calculator.GetDiscountTotal(new decimal(12.33));

            var expected = decimal.Round(result, 2, MidpointRounding.AwayFromZero);

            Assert.AreEqual(expected, result);
        }

        [Test]
        public void EnterpriseGetsEnterpriseSupplementApplied()
        {
            IDiscountRepository repo = new DiscountRepository();
            var calculator = new Calculator(repo);
            var rate = repo.GetRate().Rate;
            var supplemental = repo.GetSupplementaRate(DiscountSupplementType.Enterprise).SupplementalRate;

            var result = calculator.GetDiscountTotal(12, DiscountSupplementType.Enterprise);

            var expected = decimal.Round((decimal)12.0 * ((rate + supplemental) / (100 - (rate + supplemental))), 2, MidpointRounding.AwayFromZero);

            Assert.AreEqual(expected, result);
        }

        [Test]
        public void SmallMediumGetsSmallMediumSupplementApplied()
        {
            IDiscountRepository repo = new DiscountRepository();
            var calculator = new Calculator(repo);
            var rate = repo.GetRate().Rate;
            var supplemental = repo.GetSupplementaRate(DiscountSupplementType.SmallMed).SupplementalRate;

            var result = calculator.GetDiscountTotal(12, DiscountSupplementType.SmallMed);

            var expected = decimal.Round((decimal)12.0 * ((rate + supplemental) / (100 - (rate + supplemental))), 2, MidpointRounding.AwayFromZero);

            Assert.AreEqual(expected, result);
        }

        [Test]
        public void StandardGetsStandardSupplementApplied()
        {
            IDiscountRepository repo = new DiscountRepository();
            var calculator = new Calculator(repo);
            var rate = repo.GetRate().Rate;
            var supplemental = repo.GetSupplementaRate(DiscountSupplementType.Standard).SupplementalRate;

            var result = calculator.GetDiscountTotal(12, DiscountSupplementType.Standard);

            var expected = decimal.Round((decimal)12.0 * ((rate + supplemental) / (100 - (rate + supplemental))), 2, MidpointRounding.AwayFromZero);

            Assert.AreEqual(expected, result);
        }
    }
}
