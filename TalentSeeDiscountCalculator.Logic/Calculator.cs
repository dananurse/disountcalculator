﻿using System;
using TalentSeeDiscountCalculator.Data;

namespace TalentSeeDiscountCalculator.Logic
{
    public class Calculator
    {
        private readonly decimal _dicountRate = new decimal(20.0);

        private readonly IDiscountRepository _repo;

        public Calculator(IDiscountRepository repo)
        {
            _repo = repo;
        }

        public decimal GetDiscountTotal(decimal credits, DiscountSupplementType supplementType = DiscountSupplementType.Standard)
        {
            var rate = _repo.GetRate();

            var supplement = _repo.GetSupplementaRate(supplementType);

            var totalRate = rate.Rate + supplement.SupplementalRate;

            var discountTotal = credits * (totalRate / (100 - totalRate));

            return decimal.Round(discountTotal, 2, MidpointRounding.AwayFromZero);
        }
    }
}
